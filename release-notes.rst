0.9.4 December 29th - "works for you"
+++++++++++++++++++++++++++++++++++++

We are pleased to announce the Bitmask 0.9.4 release.  You can refer to `the
changelog`_ for the whole changeset.

This is a beta-only Encrypted Email service, with some performance improvements
in the SOLEDAD synchronization (shipping soledad 0.9.2, which needs exactly the
same version deployed in the server).

The demo provider for the Mail service you should use with this bundle is 
https://mail.bitmask.net. This provider is already pinned in Bitmask for easy
access when creating a new account from within the wizard.

Please help us test Bitmask and file any bug reports here:
https://leap.se/code/projects/report-issues, that will be a great contribution
towards future improvement!

Until the next release, see you on the intertubes, and stay safe.

The Bitmask team.

.. _`the changelog`: https://github.com/leapcode/bitmask_client/blob/0.9.4/CHANGELOG.rst
