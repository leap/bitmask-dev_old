Changelog
=====================

0.9.4 - works for you
---------------------

Features
~~~~~~~~
- `#7550 <https://leap.se/code/issues/7550>`_: Add ability to use invite codes during signup
- `#7965 <https://leap.se/code/issues/7965>`_: Add basic keymanagement to the cli.
- `#8265 <https://leap.se/code/issues/8265>`_: Add a REST API and bitmask.js library for it.
- `#8400 <https://leap.se/code/issues/8400>`_: Add manual provider registration.
- `#8435 <https://leap.se/code/issues/8435>`_: Write service tokens to a file for email clients to read.
- `#8486 <https://leap.se/code/issues/8486>`_: Fetch smtp cert automatically if missing.
- `#8487 <https://leap.se/code/issues/8487>`_: Add change password command.
- `#8488 <https://leap.se/code/issues/8488>`_: Add list users to bonafide.
- Use mail_auth token in the core instead of imap/smtp tokens.


Bugfixes
~~~~~~~~
- `#8498 <https://leap.se/code/issues/8498>`_: In case of wrong url don't leave files in the config folder.

